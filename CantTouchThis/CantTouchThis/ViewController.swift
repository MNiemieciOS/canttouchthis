//
//  ViewController.swift
//  CantTouchThis
//
//  Created by Michal Niemiec on 09/07/15.
//  Copyright © 2015 MNiemiec. All rights reserved.
//

import UIKit
import CoreBluetooth
import CoreLocation

class ViewController: UIViewController,CBPeripheralManagerDelegate  {

    
    @IBOutlet var sendInfoButton: UIButton!
    let major = CLBeaconMajorValue(12)
    let minor = CLBeaconMinorValue(12)
    let uuid = NSUUID(UUIDString: "F7826DA6-4FA2-4E98-8024-BC5B71E0893E")
    let name = "Secure iPhone"
    var peripheralManager = CBPeripheralManager()
    var advertisedData = NSMutableDictionary()
    var region = CLBeaconRegion()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        region = CLBeaconRegion(proximityUUID: self.uuid!, major: self.major, minor: self.minor, identifier: "Security")
        
        self.advertisedData = region.peripheralDataWithMeasuredPower(nil)
        
        self.advertisedData .setObject(name, forKey: CBAdvertisementDataLocalNameKey)
        self.peripheralManager = CBPeripheralManager(delegate: self, queue: nil, options: nil)

                // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func peripheralManagerDidUpdateState(peripheral: CBPeripheralManager) {
        switch peripheral.state {
        case CBPeripheralManagerState.PoweredOn:
            self.peripheralManager.startAdvertising(self.advertisedData as! [String : AnyObject])

        default :
            var a = 5
        }
    }
    @IBAction func sendInfoTapped(sender: AnyObject) {
        

    }
}

