//
//  AppDelegate.swift
//  CantTouchThisOSX
//
//  Created by Michal Niemiec on 09/07/15.
//  Copyright © 2015 MNiemiec. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

