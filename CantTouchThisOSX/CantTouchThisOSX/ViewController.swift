//
//  ViewController.swift
//  CantTouchThisOSX
//
//  Created by Michal Niemiec on 09/07/15.
//  Copyright © 2015 MNiemiec. All rights reserved.
//
import Foundation
import Cocoa
import CoreBluetooth
import CoreLocation
import AppKit
import SecurityFoundation


class ViewController: NSViewController, CBCentralManagerDelegate ,CLLocationManagerDelegate,NSTextFieldDelegate {

    @IBOutlet var showDeviceButton: NSButton!
    @IBOutlet var savePasswordButton: NSButton!
    @IBOutlet var setPasswordTextField: NSSecureTextField!
    var centralManager = CBCentralManager()
    var centralLocationManager = CLLocationManager()
    var isLock = false
    var password = String()
    var uuidList = [String]()
    
    
  
    override func viewDidLoad() {
        super.viewDidLoad()

        centralManager = CBCentralManager(delegate: self, queue: nil)
        self.centralLocationManager.delegate = self
        // Do any additional setup after loading the view.
        self.setPasswordTextField.delegate = self
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func showDeviceTapped(sender: AnyObject) {

        if !setPasswordTextField.stringValue.isEmpty {
            
            
        centralManager.scanForPeripheralsWithServices([CBUUID(string:"F7826DA6-4FA2-4E98-8024-BC5B71E0893E")] ,options: nil)
        } else {
            let myPopup: NSAlert = NSAlert()
            myPopup.informativeText = "Please put your account password"
            myPopup.messageText = "Empty password"
            myPopup.alertStyle = NSAlertStyle.WarningAlertStyle
            let res = myPopup.runModal()

        }
    }
    
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        if peripheral.name == "Secure iPhone" {
            print(RSSI.description)
            if !self.isLock && RSSI.intValue < -65 {
                
                
                let script = "tell application \"System Events\" \n start current screen saver \n delay (5) \n end tell"
                let appleScript = NSAppleScript(source: script)
                let eventResult = appleScript!.executeAndReturnError(nil)
                
                print("LOCKED")

                self.isLock = true
            }
            if  self.isLock && RSSI.intValue > -45 {
                var script = "tell application \"System Events\" \n stop current screen saver \n  end tell"
                var appleScript = NSAppleScript(source: script)
                var eventResult = appleScript!.executeAndReturnError(nil)
                
               
                script =  "tell application \"System Events\" \n keystroke \"\(setPasswordTextField.objectValue!)\" \n keystroke return \n end tell"
                appleScript = NSAppleScript(source: script)
                eventResult = appleScript!.executeAndReturnError(nil)
                self.isLock = false
                print("UNLOCKED")

            }
        }
        
    }

    

    func centralManagerDidUpdateState(central: CBCentralManager) {
        
        switch central.state {
            
        case .PoweredOn:
            print(".PoweredOn")
            
        case .PoweredOff:
            print(".PoweredOff")
            
        case .Resetting:
            print(".Resetting")
            
        case .Unauthorized:
            print(".Unauthorized")
            
        case .Unknown:
            print(".Unknown")
            
        case .Unsupported:
            print(".Unsupported")
        }
    }
    

    @IBAction func savePasswordTapped(sender: AnyObject) {
//        var myItems = [
//            AuthorizationItem(name: "com.myOrganization.myProduct.myRight1",
//                valueLength: 0, value: nil, flags: 0),
//            AuthorizationItem(name: "com.myOrganization.myProduct.myRight2",
//                valueLength: 0, value: nil, flags: 0)
//        ]
//        
//        var myRights = AuthorizationRights(count: UInt32(myItems.count), items: &myItems)
//        
//        let myFlags = AuthorizationFlags(kAuthorizationFlagDefaults |
//            kAuthorizationFlagInteractionAllowed |
//            kAuthorizationFlagExtendRights)
//        
//        
//        var authRef: AuthorizationRef = nil
//        let authFlags = AuthorizationFlags(kAuthorizationFlagDefaults)
//        let osStatus = AuthorizationCreate(&myRights, nil, authFlags, &authRef)
//    }

        
        
        
//        var myItems = [
//            AuthorizationItem(name: "com.MNiemiec.CantTouchThisOSX",
//                valueLength: 0, value: nil, flags: 0),
//            ]
//        
//        var myRights = AuthorizationRights(count: UInt32(myItems.count), items: &myItems)
//        
//        let myFlags = AuthorizationFlags(arrayLiteral: AuthorizationFlags.Defaults, AuthorizationFlags.InteractionAllowed, AuthorizationFlags.ExtendRights)
//        
//        
//        var authRef: AuthorizationRef = nil
//        let authFlags = AuthorizationFlags(arrayLiteral: AuthorizationFlags.Defaults )
//        let osStatus = AuthorizationCreate(&myRights, nil, authFlags, &authRef)
    }
        
        

}

